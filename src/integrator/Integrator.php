<?php

/**
 * (c) Rob Roy <rob@pervasivetelemetry.com.au>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrator;

use OdooClient\Client;

class Integrator
{
    protected $client;
    
    public function __construct($host, $database, $user, $password)
    {
        $this->client = new Client($host, $database, $user, $password);
    }
    
    public function getPartner($name, $ref){
        if ($ref && $name){
            $partner =$this->client->search_count("res.partner", [['name','=', $name],['ref','=',$ref]]);
        }
        else{
            $partner =$this->client->search_count("res.partner", [['name','=', $name]]);
        }
        if ($partner>0){
            if ($ref && $name){
                $partner_id =$this->client->search("res.partner", [['name','=', $name],['ref','=',$ref]], 0, 1)[0];
            }
            else{
                $partner_id =$this->client->search("res.partner", [['name','=', $name]], 0, 1)[0];
            }
        }
        else{
            if ($ref && $name){
                $data = [
                    'name' => $name,
                    'ref' => $ref,
                ];
                $partner_id =$this->client->create("res.partner", $data);
            }
            else{
                $data = [
                    'name' => $name,
                ];
                $partner_id =$this->client->create("res.partner", $data);
            }
        }
        return $partner_id;
    }
    
    public function getProduct($code){
        $product =$this->client->search_count("product.product", [['default_code','=', $code]]);
        if ($product>0) {
            $product_id = $this->client->search("product.product", [['default_code','=', $code]], 0, 1)[0];
            return $product_id;
        }
        else{
            return false;
        }
    }
    
    public function createOrder($partner, $lines, $note=""){
        $partner_id = $this->getPartner($partner['name'], $partner['alt_cms_id']);
        $order_lines=[];
        foreach ($lines as $line){
            $product_id = $this->getProduct($line['code']);
            if ($product_id){
                $list = [
                    "product_id"=>$product_id, 
                    "product_uom_qty" => $line['units'],
                ];
                array_push($order_lines, array(0, false, $list));
            }
            
        }
        $data = [
            "partner_id" => $partner_id,
            "note" => $note,
            "order_line" => $order_lines
            
        ];
        $sale_id = $this->client->create("sale.order", $data);
        return $sale_id;
    }
}
